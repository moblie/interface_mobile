import 'dart:ffi';
import 'dart:io';

import 'Runable.dart';
import 'Vahicle.dart';

class Car extends Vahicle implements Runable {
  String nickname = '';

  Car(String nickname) : super("engine") {
    this.nickname = nickname;
  }

  @override
  void startEngine() {
    print("Car : " + nickname + " startEngine");
  }

  @override
  void stopEngine() {
    print("Car : " + nickname + " stopEngine");
  }

  @override
  void raiseSpeed() {
    print("Car : " + nickname + " raiseSpeed");
  }

  @override
  void applyBreak() {
    print("Car : " + nickname + " applyBreak");
  }

  @override
  void run() {
    print("Car : " + nickname + " run");
  }
}
