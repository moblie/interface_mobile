import 'Bat.dart';
import 'Car.dart';
import 'Dog.dart';
import 'Flyable.dart';
import 'Plane.dart';
import 'package:interface_mobile/interface_mobile.dart' as interface_mobile;

void main() {
  Bat bat = Bat("GluayHom");
  Plane plane = Plane("Engine numberI");
  bat.fly();
  plane.fly();
  Dog dog = Dog("Yuki");
  Car car = Car("Cake");

  List<Flyable> flyable = [bat, plane];
  for (var f in flyable) {
    if (f is Plane) {
      Plane p = f;
      p.startEngine();
      p.run();
    }
  }
}
