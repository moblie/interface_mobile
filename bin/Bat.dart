import 'dart:ffi';
import 'dart:io';

import 'Poultry.dart';

class Bat extends Poultry {
  String nickname = '';

  Bat(String nickname) {
    this.nickname = nickname;
  }

  @override
  void eat() {
    print("Bat: " + nickname + " eat");
  }

  @override
  void speak() {
    print("Bat: " + nickname + " speak");
  }

  @override
  void sleep() {
    print("Bat: " + nickname + " sleep");
  }

  @override
  void fly() {
    print("Bat: " + nickname + " fly");
  }
}
