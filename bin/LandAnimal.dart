import 'Animal.dart';
import 'Runable.dart';

abstract class LandAnimal extends Animal implements Runable {
  LandAnimal(String name, int numberOfLeg) : super(name, numberOfLeg) {}
}
