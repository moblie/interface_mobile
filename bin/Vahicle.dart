abstract class Vahicle {
  String engine = '';

  Vahicle(String engine) {
    this.engine = engine;
  }

  String getEngine() {
    return engine;
  }

  void setEngine(String engine) {
    this.engine = engine;
  }

  void startEngine();
  void stopEngine();
  void raiseSpeed();
  void applyBreak();
}
