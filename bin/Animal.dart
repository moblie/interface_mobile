import 'package:interface_mobile/interface_mobile.dart' as interface_mobile;
import 'dart:io';

abstract class Animal {
  String name = '';
  int numberOfLeg = 0;

  Animal(String name, int numberOfLeg) {
    this.name = name;
    this.numberOfLeg = numberOfLeg;
  }

  String getName() {
    return name;
  }

  void setName(String name) {
    this.name = name;
  }

  int getNumberOfLeg() {
    return numberOfLeg;
  }

  void setNumberOfLeg(int numberrOfLeg) {
    this.numberOfLeg = numberrOfLeg;
  }

  void eat();
  void speak();
  void sleep();
}
