import 'Animal.dart';
import 'Flyable.dart';

abstract class Poultry extends Animal implements Flyable {
  Poultry() : super("Poultry", 2) {}
}
