import 'Flyable.dart';
import 'Runable.dart';
import 'Vahicle.dart';

class Plane extends Vahicle implements Flyable, Runable {
  String nickname = '';

  Plane(String nickname) : super("engine") {
    this.nickname = nickname;
  }

  @override
  void startEngine() {
    print("Plane: " + nickname + " startEngine");
  }

  @override
  void stopEngine() {
    print("Plane: " + nickname + " stopEngine");
  }

  @override
  void raiseSpeed() {
    print("Plane: " + nickname + " raiseSpeed");
  }

  @override
  void applyBreak() {
    print("Plane: " + nickname + " applyBreak");
  }

  @override
  void fly() {
    print("Plane: " + nickname + " fly");
  }

  @override
  void run() {
    print("Plane: " + nickname + " run");
  }
}
