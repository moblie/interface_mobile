import 'LandAnimal.dart';

class Dog extends LandAnimal {
  String nickname = '';

  Dog(String nickname) : super("Dog", 4) {
    this.nickname = nickname;
  }

  @override
  void eat() {
    print("Dog : " + nickname + " eat");
  }

  @override
  void speak() {
    print("Dog : " + nickname + " speak");
  }

  @override
  void sleep() {
    print("Dog : " + nickname + " sleep");
  }

  @override
  void run() {
    print("Dog : " + nickname + " run");
  }
}
